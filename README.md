# Idées / prétexte

- la communauté EVS communique et échange à travers plusieurs ateliers ayant chacun son blog et ses responsables
- chacun.e peut être intéressé.e à recevoir les infos sans forcément être inscrit comme membre participant
- la centralisation de l'inscription sur les listes de diffusion repose sur le bénévolat et le temps des responsables d'ateliers
- stagiaires et contractuel.le.s bénéficient de ces sources d'information mais ne peuvent être inscrits / désinscrits facilement des listes (cf point du TODO)
- le format de flux RSS (flux de syndication) est l'outil de choix pour la **syndication** de contenu Web
- **utiliser** et **contribuer** à l'actualisation **décentralisée** de ce fichier OPML permet à **chacun.e** l'accès aux infos

**ATTENTION** cette méthode ne remplace pas l'inscription auprès des responsables des ateliers en cas de participation
active souhaitée pour les permanents EVS.


Thèmes:
- collectif
- infobésité
- inclusivité

# URLs des Ateliers EVS

https://umr5600.cnrs.fr/fr/la-recherche/ateliers/

- [Faire Territoire, faire Société](https://umr5600.cnrs.fr/fr/la-recherche/ateliers/faire-territoire-faire-societe/); [le blog](https://atelier6.hypotheses.org/)
- [Socio-Ecosystèmes](https://umr5600.cnrs.fr/fr/la-recherche/ateliers/socio-ecosystemes/)
- [Flux Circulations](https://umr5600.cnrs.fr/fr/la-recherche/ateliers/flux-et-circulations/)
- [Objets Urbanisation](https://umr5600.cnrs.fr/fr/la-recherche/ateliers/objets-urbanisations/)
- [Santé et Environnements](https://umr5600.cnrs.fr/fr/la-recherche/ateliers/socio-ecosystemes/)
- [Spatialités numériques et Géomatique](https://umr5600.cnrs.fr/fr/la-recherche/ateliers/spatialites-numeriques-et-geomatique/)
- [Recherches en Situations Multidisciplinaires](https://umr5600.cnrs.fr/fr/recherches-en-situation-multidisciplinaire/)
- [Vie des doctorant.e.s](https://umr5600.cnrs.fr/fr/la-recherche/ateliers/vie-des-doctorants/)


# Fichier OPML et son utilisation:

Ce [fichier OPML](https://fr.wikipedia.org/wiki/Outline_Processor_Markup_Language) peut être importé dans un client (ex: Thunderbird) pour automatiser la veille des flux RSS des blogs des différents ateliers d'EVS.

Il est alors possible d'utiliser les filtres pour être alerté de nouvelles dont la pertinence est déterminée par chaque utilisateur.


<div align="center">

  <img src="captures/thunderbird_apercu_des_flux.png" width="600"/>
</div>

<div align="center">

  <img src="captures/thunderbird_gestion_des_flux.png" width="600"/>
</div>

# TODO:
- [X] ~~ajouter un flux pour Vie des Doctorant.e.s (pas trouvé sur le blog)~~
- [X] ~~ajouter le flux des new EVS~~

- [ ] ajouter le flux des commentaires aux news EVS ?? (désactivés pour l'instant)
- [ ] documenter l'import et l'utilisation du fichier OPML dans Thunderbird
- [ ] how to ? Import de l'OPML
- [ ] how to ? Export de l'OPML en cas d'actualisation
- [ ] ajouter ce fichier à la boîte à outil remise aux nouveaux arrivants ??
- [ ] comment intégrer les informations diffusées via la liste de diffusion atelier.spatialites.numeriques@univ-lyon3.fr`?